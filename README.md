# DG Developer Task

As part of the development team, we often have to interact with different apps, process the data, store the data, and finally display the data. The goal of this task is to see how a user can create and interact with multiple app using the knowledge of microservice architecture. A minimal understanding of databases and Dockers, rest API, UI is required to complete this project.

## Data Processor APP
  - Create an API endpoint that accepts JSON data and stores it in a database.
  - Before storing, log the interaction and store only data where the status is "approved".
  - Use the GET api to read data from the database.
  - Create a database table of your choice.

### Some tech notes

You could use Docker to create a Postgres db. Connect the app to the Postgres client created via Docker. It would be good to have a Swagger endpoint to run the API, but it is up to you.

Please refer to [books](/books.json) as sample test data in json format.

## Frontend APP
  - Use the GET api from process data app.
  - Dispaly data in UI.

### Additional Requirements:
While calling the GET api use the client of your choice. Display data in your format.
Sample UI screen for reference  
![sample ui screen](sampleUI.png)

### Play Framework Specifics:
Forms Mapping: Use Play Framework's forms mapping to validate and process incoming data.

Data Display: Create a simple Play Framework view to display the processed data.

## Tech Stack

**Client:** No preference.

**Server:** Preferably use Scala and the Play framework.

**Database** No preference.

## Some good to have things in project.
- Logging setup: using logback.xml.
- Using any kind of authentications while communicating with API.
- Centralized configuration setting for local, dev and live environment.

## Run Locally

Clone the project

```bash
  git clone https://link-to-project
```

